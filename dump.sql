--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: animal_classes; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.animal_classes (
    id integer NOT NULL,
    name text NOT NULL,
    can_flying boolean DEFAULT false
);


ALTER TABLE public.animal_classes OWNER TO "user";

--
-- Name: animal_classes_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

ALTER TABLE public.animal_classes ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.animal_classes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: animals; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.animals (
    id integer NOT NULL,
    name text NOT NULL,
    can_flying boolean DEFAULT false,
    legs_count integer,
    class_id integer NOT NULL
);


ALTER TABLE public.animals OWNER TO "user";

--
-- Name: animals_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

ALTER TABLE public.animals ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.animals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: cities; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.cities (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    founded_at timestamp without time zone,
    country_id integer NOT NULL
);


ALTER TABLE public.cities OWNER TO "user";

--
-- Name: cities_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

ALTER TABLE public.cities ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.cities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: countries; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.countries (
    id integer NOT NULL,
    name text NOT NULL,
    code character varying(3) NOT NULL
);


ALTER TABLE public.countries OWNER TO "user";

--
-- Name: countries_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

ALTER TABLE public.countries ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.countries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Data for Name: animal_classes; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.animal_classes (id, name, can_flying) FROM stdin;
1	Птицы	t
2	Животные	f
\.


--
-- Data for Name: animals; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.animals (id, name, can_flying, legs_count, class_id) FROM stdin;
1	Собака	f	4	2
2	Кошка	f	4	2
3	Сова	t	2	1
4	Орёл	t	2	1
\.


--
-- Data for Name: cities; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.cities (id, name, founded_at, country_id) FROM stdin;
1	Москва	1147-01-09 00:00:00	1
2	Санкт-Петербург	1703-04-16 00:00:00	1
3	Сан-Франциско	1628-01-01 00:00:00	2
4	Нью-Йорк	1535-01-18 00:00:00	2
\.


--
-- Data for Name: countries; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.countries (id, name, code) FROM stdin;
1	Россия	RUS
2	Америка	USA
\.


--
-- Name: animal_classes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.animal_classes_id_seq', 2, true);


--
-- Name: animals_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.animals_id_seq', 4, true);


--
-- Name: cities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.cities_id_seq', 4, true);


--
-- Name: countries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.countries_id_seq', 2, true);


--
-- Name: animal_classes animal_classes_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.animal_classes
    ADD CONSTRAINT animal_classes_pkey PRIMARY KEY (id);


--
-- Name: animals animals_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.animals
    ADD CONSTRAINT animals_pkey PRIMARY KEY (id);


--
-- Name: cities cities_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.cities
    ADD CONSTRAINT cities_pkey PRIMARY KEY (id);


--
-- Name: countries countries_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.countries
    ADD CONSTRAINT countries_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

