<?php

class Basket
{
    private array $positions;

    public function __construct() {
        $this->positions = array();
    }

    public function addProduct(Product $product, $quantity) {
        $basketPosition = new BasketPosition($product, $quantity);
        array_push($this->positions, $basketPosition);
    }

    public function getPrice() {
        $total_price = 0;
        foreach ($this->positions as $position) {
            $total_price += $position->getPrice();
        }

        return $total_price;
    }

    public function describe() {
        $response = "";
        foreach ($this->positions as $position) {
            $name = $position->getProduct()->getName();
            $price = $position->getProduct()->getPrice();
            $quantity = $position->getQuantity();
            $response .= "$name - $price - $quantity";
            $response .= PHP_EOL;
        }
        return $response;
    }
}