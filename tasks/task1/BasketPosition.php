<?php

class BasketPosition
{
    private Product $product;
    private int $quantity;

    public function __construct(Product $product, $quantity) {
        $this->product = $product;
        $this->quantity = $quantity;
    }

    public function getProduct() {
        return $this->product;
    }

    public function getQuantity() {
        return $this->quantity;
    }

    public function getPrice() {
        return $this->product->getPrice() * $this->quantity;
    }

}