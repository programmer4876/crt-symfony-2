<?php

class Order {
    private Basket $basket;
    private float $delivery_price;

    public function __construct(Basket $basket, float $delivery_price = 0) {
        $this->basket = $basket;
        $this->delivery_price = $delivery_price;
    }

    public function getBasket() {
        return $this->basket;
    }

    public function getPrice() {
        return $this->basket->getPrice() + $this->delivery_price;
    }
}