<?php
require_once 'Basket.php';
require_once 'BasketPosition.php';
require_once 'Order.php';
require_once 'Product.php';

$basket = new Basket();
$basket->addProduct(new Product('Карандаш', 10), 12);
$basket->addProduct(new Product('Ручка', 15), 10);
$basket->addProduct(new Product('Линейка', 20), 5);
$basket->addProduct(new Product('Пенал', 200), 3);
$basket->addProduct(new Product('Интерактивная доска', 5000), 1);

$order = new Order($basket, 7500);

?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Задача 1</title>
</head>
<body>
<button onclick="window.history.back()">Назад</button>
<p>
    Заказ, на сумму - <?php echo $order->getPrice()?>
    Состав: <br><?php echo $order->getBasket()->describe(); ?>
</p>
</body>
</html>
