<?php

class User
{
    private int $id;
    private int $age;
    private string $name;
    private string $email;

    public function __construct(int $id, int $age, string $name, string $email) {
        $this->id = $id;
        $this->age = $age;
        $this->name = $name;
        $this->email = $email;
    }

    public function getId() {
        return $this->id;
    }

    public function getAge() {
        return $this->age;
    }

    public function getName() {
        return $this->name;
    }

    public function getEmail() {
        return $this->email;
    }

    public function load(int $id): bool {
        return $id == 0 ? false : true;
    }

    public function save(): bool {
        return rand(0,1) == 1;
    }
}