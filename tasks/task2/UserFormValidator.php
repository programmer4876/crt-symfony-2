<?php

class UserFormValidator
{
    static function validate(array $data): bool {
        if (!isset($data['name']) || empty($data['name']))
            throw new \Exception('Имя не должно быть пустым');
        if (!isset($data['age']) || intval($data['age']) < 18 )
            throw new \Exception('Вам должно быть не менее 18 лет :(');
        if (!isset($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL))
            throw new \Exception('Вы ввели некорректный Email!');

        return true;
    }
}