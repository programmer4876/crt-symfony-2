<?php

require_once 'User.php';
require_once 'UserFormValidator.php';

session_start();
$_SESSION["email"] = $_POST["email"] ?? "";
$_SESSION["age"] = $_POST["age"] ?? "";
$_SESSION["name"] = $_POST["name"] ?? "";
$_SESSION["id"] = $_POST["id"] ?? "";


if (!empty($_POST)) {
    try {
        $user = new User((int)$_POST["id"], (int)$_POST["age"], $_POST["name"], $_POST["email"]);
        if ($user->load($user->getId())) {
            echo "С id пользователя всё в порядке<br/>";
            $userData = array('id' => $user->getId(), 'age' => $user->getAge(), 'name' => $user->getName(), 'email' => $user->getEmail());
            if (UserFormValidator::validate($userData)) {
                if($user->save([$user])){
                    echo "Пользователь сохранен<br/>";
                    clearSession();
                } else {
                    throw new Error("Ошибка. Пользователь не сохранен");
                }
            }
        }
        else {
            echo "У пользователя не может быть id = 0<br/>";
        }

    } catch (Error $e) {
        echo $e->getMessage();
    }
}

function clearSession(){
    $_SESSION["email"]="";
    $_SESSION["age"]="";
    $_SESSION["name"]="";
    $_SESSION["id"]="";
}
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Задача 2</title>
</head>
<body>
<button onclick="window.history.back()">Назад</button>
<br>
<a>Введите данные пользователя</a>
<form method="post">
    <p>ID: <input type="text" name="id" value="<?php echo $_SESSION['id']; ?>"/></p>
    <p>Имя: <input type="text" name="name" value="<?php echo $_SESSION['name']; ?>"/></p>
    <p>Возраст: <input type="text" name="age" value="<?php echo $_SESSION['age']; ?>"/></p>
    <p>Еmail: <input type="text" name="email" value="<?php echo $_SESSION['email']; ?>"/></p>
    <p><input type="submit"/></p>
</form>
</body>
</html>