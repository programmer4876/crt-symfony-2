<?php

$table_names = array('animals', 'animal_classes', 'cities', 'countries');

$dbconn = pg_connect("host=172.21.0.2 dbname=crt user=user password=passwd");

foreach ($table_names as $table_name) {
    $query = "SELECT * FROM " . $table_name;
    $data = pg_query($query) or die("Ошибка запроса: " . pg_last_error());
    echo "<table>\n";
    while ($line = pg_fetch_array($data, null, PGSQL_ASSOC)) {
        echo "\t<tr>\n";
        foreach ($line as $col_value) {
            echo "\t\t<td>$col_value</td>\n";
        }
        echo "\t</tr>\n";
    }
    echo "</table>\n";
}
pg_close($dbconn);